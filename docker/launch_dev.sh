#!/bin/bash

# @blame R. Matt McCann <mccann.matt@gmail.com>
# @brief Launches the user into the developer container
# @copyright &copy; 2017 Human-Proof Corp.

docker run --rm -p 8980:8980 -p 8981:8981 -w /human-proof -itv $PWD:/human-proof human-proof/web-app
