var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var helpers = require('./helpers');
const path = require('path');
var PrerenderSpaPlugin = require('prerender-spa-plugin');

const AotPlugin = require('@ngtools/webpack').AotPlugin;

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge(commonConfig, {
      devtool: 'source-map',

      module: {
          rules: [
              { test: /\.ts$/, loaders: ['@ngtools/webpack'] },
          ]
      },

      output: {
        path: helpers.root('dist'),
        publicPath: '/',
        filename: '[name].[chunkhash].js'
      },

      plugins: [
            new AotPlugin({
                tsConfigPath: './tsconfig.json',
                entryModule: helpers.root('app/app.module#AppModule')
            }),

            new webpack.NoEmitOnErrorsPlugin(),

            new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
                mangle: {
                    keep_fnames: true
                }
            }),

            new ExtractTextPlugin('[name].[hash].css'),

            new webpack.DefinePlugin({
                'process.env': {
                    'ENV': JSON.stringify(ENV),
                    'API_SERVER': JSON.stringify('https://demo-api.bitanchor.io/api'),
                }
            }),

            new webpack.LoaderOptionsPlugin({
                htmlLoader: {
                    minimize: false // workaround for ng2
                }
            }),

            new HtmlWebpackPlugin({
                inject: 'head',
                chunksSortMode: 'dependency',
                template: 'index.ejs',
                minify: {}
            }),

            // TOOD(mmccann) - Prerender is failing without explanation
            //new PrerenderSpaPlugin(
            //    path.resolve(__dirname, '../dist'),
            //    ['/', '/portal']
            //)
      ]
});
