var webpack = require('webpack');
var copyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var FaviconsWebpackPlugin = require('favicons-webpack-plugin');
var helpers = require('./helpers');
const path = require('path');

module.exports = {
    entry: {
        'polyfills': './app/polyfills.ts',
        'vendor': [
            './app/vendor.ts'
        ],
        'app': './app/main.ts',
        'style': [
            './app/kitchen-sink.scss',
        ]
    },

  resolve: {
    extensions: ['.ts', '.js'],
  },

    module: {
        rules: [
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: function (file) {
                    if (file.endsWith('kitchen-sink.scss')) {
                        return true;
                    }
                },
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            },
            {
                test: function (file) {
                    if (file.endsWith('kitchen-sink.scss')) {
                        return false;
                    } else if (file.endsWith('scss')) {
                        return true;
                    }
                },
                loaders: ['to-string-loader', 'css-loader', 'sass-loader']
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            }
        ]
    },

  plugins: [
    // Workaround for angular/angular#11580
    new webpack.ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        helpers.root('./app'), // location of your src
        {} // a map of your routes
    ),

    new webpack.optimize.CommonsChunkPlugin({
        name: ['app', 'vendor', 'polyfills', 'style']
    }),

    new webpack.ProvidePlugin({
        jQuery: 'jquery',
        $: 'jquery',
        jquery: 'jquery'
    }),

    //new FaviconsWebpackPlugin('./img/favicon.png')
  ]
};
