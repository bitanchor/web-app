var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var commonConfig = require('./webpack.common.js');
var helpers = require('./helpers');

module.exports = webpackMerge(commonConfig, {
    devtool: 'cheap-module-eval-source-map',

    module: {
        rules: [
            {
                test: /\.ts$/,
                loaders: [{
                    loader: 'awesome-typescript-loader',
                    options: { configFileName: helpers.root('', 'tsconfig.json') }
                } , 'angular2-template-loader', 'angular-router-loader']
            },
        ]
    },

    output: {
        path: helpers.root('dist'),
        publicPath: 'http://localhost:8981/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    plugins: [
        new ExtractTextPlugin('[name].css'),

        new webpack.DefinePlugin({
            'process.env': {
                'API_SERVER': JSON.stringify('http://localhost:8982')
            }
        }),

        new HtmlWebpackPlugin({
            template: 'index.ejs',
        }),
    ],

    devServer: {
        historyApiFallback: true,
        stats: 'minimal'
    }
});
