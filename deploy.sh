#!/bin/bash

# @blame R. Matt mccann
# @brief Deploys the web app to production - run with care!
# @copyright &copy; 2017 Human-Proof Corp.

# Build the production version of the web-app
npm run build_prod

# Push the built web-app to S3
aws s3 cp dist s3://human-proof-demo/ --recursive --region=us-east-2
aws s3 cp graphics s3://human-proof-demo/graphics/ --recursive --region=us-east-2
aws s3 cp style.css s3://human-proof-demo/ --region=us-east-2
