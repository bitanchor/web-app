/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Defines the root Angular module for the application
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";

import { AccountModule } from "./account/account.module";
import { AdminModule } from "./admin/admin.module";
import { AppEntryComponent } from "./app-entry.component";
import { AppRoutingModule } from "./app-routing.module";
import { SharedModule } from "./misc/shared.module";
import { PortalModule } from "./portal/portal.module";


export let APP_MODULE_DEF = {
    imports: [
        AccountModule,
        AdminModule,
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        PortalModule,
        ReactiveFormsModule,
        SharedModule,
    ],
    declarations: [
        AppEntryComponent
    ],
    providers: [

    ],
    bootstrap: [ AppEntryComponent ]
};

@NgModule(APP_MODULE_DEF)
export class AppModule { }
