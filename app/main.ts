/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Bootstraps the application to run in the browser
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { enableProdMode } from "@angular/core";

import { AppModule } from "./app.module";


if (process.env.ENV === "production") {
    enableProdMode();
}

document.addEventListener("DOMContentLoaded", function() {
    platformBrowserDynamic()
        .bootstrapModule(AppModule);
});
