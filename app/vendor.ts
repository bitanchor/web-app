// Angular
import "@angular/animations";
import "@angular/core";
import "@angular/forms";
import "@angular/http";
import "@angular/platform-browser";
import "@angular/platform-browser-dynamic";
import "@angular/common";
import "@angular/router";

// RxJs
import "rxjs";

// Others
import "jquery";
import "bootstrap";
