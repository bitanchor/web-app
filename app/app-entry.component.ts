/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Entry point for the app; Simples hands control over to the router
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { Component } from "@angular/core";

import { AuthService } from "./account/auth.service";

@Component({
    selector: "app-entry",
    templateUrl: "app-entry.component.html",
    styleUrls: ["./app-entry.component.scss"]
})
export class AppEntryComponent {

    constructor(private _authService: AuthService) { }

    isInContext(): boolean {
        return this._authService.is_in_context;
    }

    toggleLocationContext(): void {
        this._authService.is_in_context = !this._authService.is_in_context;
    }

}
