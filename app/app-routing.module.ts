/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Router logic for the App module
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { LoginComponent } from "./account/login.component";
import { TrackDbComponent } from "./admin/track-db.component";
import { TrackFileComponent } from "./admin/track-file.component";
import { TrackUserComponent } from "./admin/track-user.component";
import { PortalComponent } from "./portal/portal.component";

const routes: Routes = [
    { path: "", component: LoginComponent },
    { path: "admin/track-db", component: TrackDbComponent },
    { path: "admin/track_db", component: TrackDbComponent },
    { path: "admin/track-file", component: TrackFileComponent },
    { path: "admin/track_file", component: TrackFileComponent },
    { path: "admin/track-user", component: TrackUserComponent },
    { path: "admin/track_user", component: TrackUserComponent },
    { path: "portal", component: PortalComponent },
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }
