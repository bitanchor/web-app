/***********************************************************************************************************************
 * @blame R. Matt McCann
 * @brief Module for the portal services & ui
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";

import { SharedModule } from "../misc/shared.module";
import { PortalComponent } from "./portal.component";

export let MODULE_DEF = {
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [
        PortalComponent
    ],
    exports: [
        PortalComponent
    ],
    providers: [
    ]
};

@NgModule(MODULE_DEF)
export class PortalModule { }
