/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Basic vendor post-auth portal page
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs/Rx";

import { AuthService } from "../account/auth.service";


@Component({
    selector: "portal",
    templateUrl: "portal.component.html"
})
export class PortalComponent implements OnInit {
    _showContents = false;

    constructor(
        private _authService: AuthService,
        private _router: Router
    ) { }

    ngOnInit(): void {
        if (localStorage.getItem("is_logged_in") !== "true") {
            this._router.navigate(["/"]);
        } else {
            this._showContents = true;
        }

        let me = this;
        let timer = Observable.timer(0, 500)
            .subscribe(t => {
                if (!me._authService.is_in_context) {
                    timer.unsubscribe();
                    me._router.navigate(["/"], { queryParams: {context_logout: 1} });
                }
            });
    }
}
