/**
 * @blame TODO
 * @brief Controller for the admin's "Track DB" view
 * @copyright &copy; 2017 Human-Proof Corp.
 */

import { Component } from "@angular/core";

import { TrackDbService } from "./track-db.service";

@Component({
    selector: "admin-track-db",
    templateUrl: "track-db.component.html"
})
export class TrackDbComponent {

    constructor(
        private trackService: TrackDbService
    ) { }
}
