/**
 * @blame R. Matt McCann
 * @brief Local model definition for the User record type.
 * @copyright &copy; 2017 Human-Proof Corp.
 */

export interface User {
    /** Tracking ID of the user. */
    id: string;

    /** Whether or not the user account has been authenticated. */
    is_authenticated: boolean;

    /** Whether or not the user account is currently locked. */
    is_locked: boolean;

    /** Name of the user. */
    name: string;

    /** Profile picture of the user. */
    profile_url: string;

    /** Whether or not the user account was locked due to suspicious behavior autolocking. */
    was_autolocked: boolean;
};
