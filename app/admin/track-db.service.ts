/**
 * @blame TODO
 * @brief Service for accessing the database access tracking api
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { CrudService } from "../misc/crud.service";

/** TOOD(mmccann): This shouldn't be here - but the code structure isn't quite fleshed out yet. */
class Database {
    // TODO(mmccann): Fill me with properties
};


@Injectable()
export class TrackDbService extends CrudService<Database> {
    constructor(http: Http) {
        super(http, "/admin/database");
    }
};
