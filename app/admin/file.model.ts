/**
 * @blame R. Matt McCann
 * @brief Local model definition for the File record type.
 * @copyright &copy; 2017 Human-Proof Corp.
 */

export interface File {
    /** Permitted location context for consuming the file. */
    context: string;

    /** Tracking ID of the file. */
    id?: string;

    /** Whether or not the file is currently locked. */
    is_locked: boolean;

    /** Name of the file. */
    name: string;
};
