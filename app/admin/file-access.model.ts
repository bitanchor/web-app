/**
 * @blame R. Matt McCann
 * @brief Local model definition for the FileAccess record type.
 * @copyright &copy; 2017 Human-Proof Corp.
 */

export interface FileAccess {
    /** Action attempted by the user accessing the file. */
    action: string;

    /** Location context in which the file was accessed. */
    context: string;

    /** Tracking ID of the file that was accessed. */
    file_id: string;

    /** Name of the file being accessed. */
    file_name: string;

    /** Unique tracking ID of the file access record. */
    id: string;

    /** Approximate latitude where the file access occurred. */
    location_lat: number;

    /** Approximate longitude where the file access occurred. */
    location_lon: number;

    /** Time when the access occurred. */
    time: string;

    /** Tracking ID of the user who accessed the file. */
    user_id: string;

    /** Name of the user who accessed the file. */
    user_name: string;

    /** Whether or not the action was permitted. */
    was_permitted: boolean;

    warning: boolean;
};
