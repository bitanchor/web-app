/**
 * @blame TODO
 * @brief Controller for the admin's "Track User" view
 * @copyright &copy; 2017 Human-Proof Corp.
 */

import { Component } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { FileAccess } from "./file-access.model";
import { FileAccessService } from "./file-access.service";
import { User } from "./user.model";
import { UserService } from "./user.service";

@Component({
    selector: "admin-track-user",
    templateUrl: "track-user.component.html",
    styleUrls: ["./track-user.component.scss"]
})
export class TrackUserComponent {
    /** List of the most recent file accesses for the user. */
    fileAccesses = new Array<FileAccess>();

    /** User being viewed. */
    user: User;

    /** NOTE: This is being hard-coded for the demo as we are only ever viewing the one user. */
    userId = "0";

    constructor(
        private fileAccessService: FileAccessService,
        private userService: UserService
    ) { }

    /** Initializes the demo data. */
    initDemoData(): void {
        console.info("Initializing demo data...");
        console.info("Cache bust...");

        // Create the user account
        this.userService.create({
                "id": this.userId,
                "is_authenticated": false,
                "is_locked": false,
                "name": "Matt McCann",
                "profile_url": "todo",
                "was_autolocked": false
            })
            .then((user: User) => {
                this.user = user;
            });

        // Create the file access log
        this.fileAccessService.create({
            "action": "read",
            "context": "Nationwide VPN",
            "file_id": "1",
            "file_name": "QuarterlyResults.xls",
            "id": "0",
            "location_lat": 40.146556,
            "location_lon": -83.096562,
            "time": "2017/10/18 12:59:30",
            "user_id": "0",
            "user_name": "Matt McCann",
            "was_permitted": true,
            "warning": false
        })
        .then((fileAccess: FileAccess) => {
            this.fileAccessService.create({
                "action": "edit",
                "context": "Nationwide VPN",
                "file_id": "2",
                "file_name": "CorporatePolicy.pdf",
                "id": "0",
                "location_lat": 40.146556,
                "location_lon": -83.096562,
                "time": "2017/10/12 06:59:30",
                "user_id": "0",
                "user_name": "Matt McCann",
                "was_permitted": false,
                "warning": false
            });
        });
    }

    /** Fetch the user data and their access logs. */
    fetchUserData(isRetry: boolean = false): void {
        // Fetch the user record data
        this.userService.read(this.userId)
            .then((user: User) => {
                console.info(user);
                this.user = user;

                // Fetch the file access data
                this.userService.listFileAccess(this.userId)
                    .then((fileAccesses: Array<FileAccess>) => {
                        console.info("woop woop");
                        fileAccesses.reverse();
                        this.fileAccesses = fileAccesses;

                        if (this.fileAccesses.length > 10) {
                            for (let iter = 0; iter < this.fileAccesses.length - 10; iter++) {
                                this.fileAccesses[iter].warning = true;
                            }
                        }
                    })
                    .catch(error => {
                        console.error(error);
                    });
            })
            .catch(error => {
                // If we failed to fetch records, init the data
                if (!isRetry) {
                    this.initDemoData();

                    // Try to fetch the user data again, but only once
                    this.fetchUserData(true);
                }
            });
    }

    /** Fetch the user record on component init. */
    ngOnInit(): void {
        // Fetch the user data
        this.fetchUserData();

        // Setup a period refresh of the record state
        let me = this;
        Observable.timer(10000, 1000)
             .subscribe(t => me.fetchUserData());
    }

    /** Updates the user's is_locked status when checkbox is clicked. */
    toggleUserIsLocked(): void {
        // Update the user status and clear the autolock status
        this.user.is_locked = !this.user.is_locked;
        this.user.was_autolocked = false;

        // Update the record
        this.userService.update(this.userId, this.user)
            .then(result => console.info(result))
            .catch(error => console.error(error));
    }
}
