/**
 * @blame TODO
 * @brief Service for accessing the file access tracking api
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { CrudService } from "../misc/crud.service";
import { FileAccess } from "./file-access.model";

/** Service for accessing the file access records api. */
@Injectable()
export class FileAccessService extends CrudService<FileAccess> {
    constructor(http: Http) {
        super(http, "/file/access/");
    }
}
