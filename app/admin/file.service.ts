/**
 * @blame R. Matt McCann
 * @brief Service for access the file api
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { CrudService } from "../misc/crud.service";
import { File } from "./file.model";
import { FileAccess } from "./file-access.model";

@Injectable()
export class FileService extends CrudService<File> {
    private http: Http;

    constructor(http: Http) {
        super(http, "/file/");

        this.http = http;
    }

    /**
     * Fetches the latest file accesses for the specified file.
     *
     * @param file_id Tracking ID of the file whose records should be fetched
     * @returns List of recent file accesses
     */
    listFileAccess(file_id: string): Promise<Array<FileAccess>> {
        const url = `${this._urlBase}${file_id}/list_file_access/`;

        return this.http
            .post(url, { withCredentials: true})
            .toPromise()
            .then(response => response.json())
            .catch(error => Promise.reject(error.json()));
    }
}
