/**
 * @blame TODO
 * @brief Controller for the admin"s "Track File" view
 * @copyright &copy; 2017 Human-Proof Corp.
 */

import "rxjs/add/operator/map";

import { Component, OnInit, DoCheck } from "@angular/core";
import { Observable } from "rxjs/Observable";

import { File } from "./file.model";
import { FileAccess } from "./file-access.model";
import { FileAccessService } from "./file-access.service";
import { FileService } from "./file.service";


/** Controller for the admin"s "Track File" view. */
@Component({
    selector: "admin-track-file",
    templateUrl: "track-file.component.html",
    styleUrls: ["./track-file.component.scss"]
})

export class TrackFileComponent implements DoCheck, OnInit {
    /** File being viewed. */
    file: File;

    /** List of the most recent file accesses for the file. */
    fileAccesses = new Array<FileAccess>();

    /** NOTE: This is being hard-coded for the demo as we are only ever viewing the one file. */
    fileId = "0";

    /** Last time the file was accessed. */
    lastAccessTime = "";

    /** Who last accessed the file. */
    lastAccessUserName = "";

    /**  Latest latitude and longitude **/
    latestLatitude = 0;
    latestLongitude = 0;

    dkBlue = "#94aacc";
    ltBlue = "#c9d4e5";
    green = "#d5e9b9";
    /**Map styles**/
    mapStyles = [
      { // background
        elementType: "labels.text.fill",
        stylers: [{color: "#2e3e56"}]
      },
      { // background
        featureType: "landscape",
        stylers: [{color: this.dkBlue}]
      },
      { // background
        featureType: "poi.park",
        stylers: [{color: this.green}]
      },
      { // background
        featureType: "administrative",
        elementType: "geometry",
        stylers: [{color: this.green}]
      },
      { // building colors
        featureType: "landscape.man_made",
        elementType: "geometry",
        stylers: [{color: this.ltBlue}]
      },
      { // turn off business,medical, etc. markers
        featureType: "poi",
        elementType: "labels",
        stylers: [
          { visibility: "off" }
        ]
      },
      { // turn off business,medical, etc. markers
        featureType: "road.highway",
        elementType: "geometry",
        stylers: [
          {color: "#516687"}
        ]
      }
    ];

    constructor(
        private fileAccessService: FileAccessService,
        private fileService: FileService
    ) { }

    /** Initializes the demo data. */
    initDemoData(): void {
        console.info("Initializing demo data...");

        // Create the file record
        this.fileService.create({
            "id": this.fileId,
            "context": "Nationwide VPN",
            "is_locked": false,
            "name": "CustomerPII.doc"
        })
        .then((file: File) => {
            console.info("Created demo file record!");
            this.file = file;
        });

        // Create the file acccess log
        this.fileAccessService.create({
            "action": "read",
            "context": "Nationwide VPN",
            "file_id": this.fileId,
            "file_name": "CustomerPII.doc",
            "id": "0",
            "location_lat": 40.146556,
            "location_lon": -83.096562,
            "time": "2017/10/16 09:59:30",
            "user_id": "0",
            "user_name": "Matt McCann",
            "was_permitted": true,
            "warning": false
        })
        .then((fileAccess: FileAccess) => {
            console.info("Created first file access record!");
            this.fileAccesses.push(fileAccess);

            this.fileAccessService.create({
                "action": "edit",
                "context": "Nationwide VPN",
                "file_id": this.fileId,
                "file_name": "CustomerPII.doc",
                "id": "0",
                "location_lat": 40.146556,
                "location_lon": -83.096562,
                "time": "2017/10/17 11:59:30",
                "user_id": "0",
                "user_name": "Matt McCann",
                "was_permitted": false,
                "warning": false
            })
            .then((fileAccess: FileAccess) => {
                console.info("Created second file access record!");
                this.fileAccesses.push(fileAccess);

                this.fileAccessService.create({
                    "action": "edit",
                    "context": "Nationwide VPN",
                    "file_id": this.fileId,
                    "file_name": "CustomerPII.doc",
                    "id": "0",
                    "location_lat": 40.146556,
                    "location_lon": -83.096562,
                    "time": "2017/10/18 08:59:30",
                    "user_id": "0",
                    "user_name": "Matt McCann",
                    "was_permitted": true,
                    "warning": false
                })
                .then((fileAccess: FileAccess) => {
                    this.fileAccesses.push(fileAccess);
                    console.info(this.fileAccesses);
                });
            });
        });
    }

    /** Fetch the file record being viewed and its access logs. */
    fetchFileData(): void {
        // Fetch the file record data
        this.fileService.read(this.fileId)
            .then((file: File) => {
                console.info(file);
                this.file = file;

                // Fetch the file access data
                this.fileService.listFileAccess(this.fileId)
                    .then((fileAccesses: Array<FileAccess>) => {
                        console.info(fileAccesses);
                        fileAccesses.reverse();
                        this.fileAccesses = fileAccesses;
                    })
                    .catch(error => {
                        console.error(error);
                    });
            })
            .catch(error => {
                this.initDemoData();
            });
    }

    /** Update the derived values after a model update. */
    ngDoCheck(): void {
        if (this.fileAccesses.length > 0) {
            this.lastAccessTime = this.fileAccesses[0]["time"];
            this.latestLatitude = this.fileAccesses[0]["location_lat"];
            this.latestLongitude = this.fileAccesses[0]["location_lon"];
        } else {
            this.lastAccessTime = "";
        }

        if (this.fileAccesses.length > 0) {
            this.lastAccessUserName = this.fileAccesses[0]["user_name"];
        } else {
            this.lastAccessUserName = "";
        }
    }

    /** Fetch the file record on component init. */
    ngOnInit(): void {
        // Fetch the file record being viewed
        this.fetchFileData();

        // Setup a period refresh of the record state
        let me = this;
        Observable.timer(10000, 1000)
            .subscribe(t => me.fetchFileData());
    }

    /** Updates the file's is_locked status when checkbox is clicked. */
    toggleFileIsLocked(): void {
        // Update the file status
        this.file.is_locked = !this.file.is_locked;

        // Update the record
        this.fileService.update(this.fileId, this.file)
            .then(result => console.info(result))
            .catch(error => console.error(error));
    }
}
