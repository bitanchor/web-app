/**
 * @blame TODO
 * @brief Service for accessing the user access tracking api
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from "@angular/core";
import { Http } from "@angular/http";

import { CrudService } from "../misc/crud.service";
import { FileAccess } from "./file-access.model";
import { User } from "./user.model";

@Injectable()
export class UserService extends CrudService<User> {
    private http: Http;

    constructor(http: Http) {
        super(http, "/user/");

        this.http = http;
    }

    /**
     * Fetches the latest file accesses for the specified user.
     *
     * @param user_id Tracking ID of the user whose records should be fetched
     * @returns List of recent file accesses
     */
    listFileAccess(user_id: string): Promise<Array<FileAccess> > {
        const url = `${this._urlBase}${user_id}/list_file_access/`;

        return this.http
            .post(url, { withCredentials: true})
            .toPromise()
            .then(response => response.json())
            .catch(error => Promise.reject(error.json()));
    }
}
