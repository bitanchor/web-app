/**
 * @blame R. Matt McCann
 * @brief Module for the admin services & ui
 * @copyright &copy; 2017 Human-Proof Corp.
 */

import { AgmCoreModule } from "@agm/core";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedModule } from "../misc/shared.module";
import { FileAccessService } from "./file-access.service";
import { FileService } from "./file.service";
import { TrackDbService } from "./track-db.service";
import { TrackDbComponent } from "./track-db.component";
import { TrackFileComponent } from "./track-file.component";
import { TrackUserComponent } from "./track-user.component";
import { UserService } from "./user.service";


export let MODULE_DEF = {
    imports: [
        CommonModule,
        SharedModule,
        AgmCoreModule.forRoot({
          apiKey: "AIzaSyDRv81m13qpF7cUjg9KKkwC9X9eGIvWRyk",
        })
    ],
    declarations: [
        TrackDbComponent,
        TrackFileComponent,
        TrackUserComponent
    ],
    exports: [
        TrackDbComponent,
        TrackFileComponent,
        TrackUserComponent
    ],
    providers: [
        TrackDbService,
        FileAccessService,
        FileService,
        UserService
    ]
};

@NgModule(MODULE_DEF)
export class AdminModule { }
