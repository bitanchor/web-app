/**
 * @author R. Matt McCann
 * @brief An error message input feedback
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Component, Input } from "@angular/core";

import { FieldState } from "./form-builder.service";


@Component({
    selector: "form-field-errors",
    template: `
        <div *ngFor="let error of field.error" class="help-block">
            <span>{{ error }}</span>
        </div>
    `
})
export class FormFieldErrorsComponent {
    @Input() field: FieldState;
}
