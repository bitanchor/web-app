/**
 * @author R. Matt McCann
 * @brief Unit tests for the CrudService
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from "@angular/core";
import { async, TestBed, inject } from "@angular/core/testing";
import { Http, HttpModule, RequestMethod, Response, ResponseOptions, XHRBackend } from "@angular/http";
import { MockBackend } from "@angular/http/testing";

import { CrudService } from "./crud.service";


@Injectable()
class CrudServiceImpl extends CrudService<string> {
    constructor(http: Http) {
        super(http, "/impl/");
    }
};


describe("CrudService", () => {
    let crudService, mockBackend;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ HttpModule ],
            providers: [
                CrudServiceImpl,
                { provide: XHRBackend, useClass: MockBackend }
            ]
        });
    });

    beforeEach(inject([CrudServiceImpl, XHRBackend], (_crudService, _mockBackend) => {
        crudService = _crudService;
        mockBackend = _mockBackend;
    }));

    describe("create", () => {
        it("calls the api correctly", async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual("http://localhost:8001/impl/");
                expect(conn.request.method).toEqual(RequestMethod.Post);
                expect(conn.request.headers.get("Content-Type")).toEqual("application/json");
                expect(conn.request.withCredentials).toBe(true);
                expect(conn.request.getBody()).toEqual(JSON.stringify("something"));
            });

            crudService.create("something");
        }));

        it("returns the created object on success", async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify("something")})));
            });

            crudService.create("something").then(
                (response) => { expect(response).toEqual("something"); },
                (error) => { fail("should not have rejected the resolved response"); }
            );
        }));

        it("returns the error on failure", async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify("error")})));
            });

            crudService.create("something").then(
                (response) => { fail("should not have resolved the error"); },
                (error) => { expect(error).toEqual("error"); }
            );
        }));
    });

    describe("read", () => {
        it("calls the api correctly", async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual("http://localhost:8001/impl/id/");
                expect(conn.request.method).toEqual(RequestMethod.Get);
                expect(conn.request.withCredentials).toBe(true);
            });

            crudService.read("id");
        }));

        it("returns the read object on success", async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify("something")})));
            });

            crudService.read("id").then(
                (response) => { expect(response).toEqual("something"); },
                (error) => { fail("should not have rejected read object"); }
            );
        }));

        it("returns the error on failure", async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify("error")})));
            });

            crudService.read("id").then(
                (response) => { fail("should have rejected error"); },
                (error) => { expect(error).toEqual("error"); }
            );
        }));
    });

    describe("update", () => {
        it("calls the api correctly", async(() => {
            mockBackend.connections.subscribe(conn => {
                expect(conn.request.url).toEqual("http://localhost:8001/impl/id/");
                expect(conn.request.method).toEqual(RequestMethod.Put);
                expect(conn.request.headers.get("Content-Type")).toEqual("application/json");
                expect(conn.request.withCredentials).toBe(true);
                expect(conn.request.getBody()).toEqual(JSON.stringify("something"));
            });

            crudService.update("id", "something");
        }));

        it("returns the updated object on success", async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockRespond(new Response(new ResponseOptions({body: JSON.stringify("something")})));
            });

            crudService.update("id", "something").then(
                (response) => { expect(response).toEqual("something"); },
                (error) => { fail("should not have rejected the resolved response"); }
            );
        }));

        it("returns the error on failure", async(() => {
            mockBackend.connections.subscribe(conn => {
                conn.mockError(new Response(new ResponseOptions({body: JSON.stringify("error")})));
            });

            crudService.update("id", "something").then(
                (response) => { fail("should not have resolved the error"); },
                (error) => { expect(error).toEqual("error"); }
            );
        }));
    });
});
