/**
 * @author R. Matt McCann
 * @brief Base class that manages state for edit forms living inside of a modal
 * @copyright &copy; 2017 R. Matt McCann
 */

import { EventEmitter, OnInit, Output } from "@angular/core";
import { FormGroup } from "@angular/forms";

import { FormBuilderService, FieldState } from "./form-builder.service";


/** Base class that manages state for edit forms living outside of a modal */
export abstract class ModalFormComponent implements OnInit {
    /** Form object used to edit the target object. */
    _form: FormGroup;

    /** Helps build the form state. */
    _formBuilder: FormBuilderService;

    /** Validity state of the form object. */
    _formState: Map<string, FieldState>;

    /** Event fired when the edit modal is ready to be closed. */
    @Output() onClose = new EventEmitter();

    /** Marked as true when an unexpected error occurs. */
    _unexpectedError = false;

    /** Error messages displayed when the form validity is violated. */
    _validationMessages = new Map<string, Map<string, string>>();

    constructor(formBuilder?: FormBuilderService) {
        this._formBuilder = formBuilder;
    }

    /** Called when the user dismisses the modal. */
    dismiss(): void {
        // Reset the state of the component
        this._init();
    }

    /** Initializes the component state. */
    abstract _init(): void;

    /** Initializes the component on creation. */
    ngOnInit(): void {
        this._init();
    }

    /** Process the user clicking the cancel button. */
    _onCancel(): void {
        // Reset the state of the component
        this._init();

        // Close the edit modal
        this.onClose.emit();
    }

    /** Process changes to the form. */
    _onFormChanged(): void {
        // Clear the unexpected error message
        this._unexpectedError = false;

        // Use the standard form processor
        this._formBuilder.onFormChanged(this._form, this._formState, this._validationMessages);
    }

    /** Subscribes to state changes on the form. */
    _subscribeToFormChanges(debounceTime?: number): void {
        if (debounceTime === undefined) {
            debounceTime = 1000;
        }

        this._form.valueChanges
            .debounceTime(debounceTime)
            .subscribe(data => this._onFormChanged());
    }
};
