/**
 * @author R. Matt McCann
 * @brief Configures shared dependency
 * @copyright &copy; 2017 R. Matt McCann
 */

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { FormBuilderService } from "./form-builder.service";
import { FormFieldErrorsComponent } from "./form-field-errors.component";


export let MODULE_DEF = {
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule
    ],
    declarations: [
        FormFieldErrorsComponent
    ],
    exports: [
        FormFieldErrorsComponent
    ],
    providers: [
        FormBuilderService,
    ]
};

@NgModule(MODULE_DEF)
export class SharedModule { }
