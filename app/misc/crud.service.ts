/**
 * @author R. Matt McCann
 * @brief Generic CRUD service base
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Headers, Http } from "@angular/http";

/** Generic CRUD service base class. */
export class CrudService<T> {
    _headers = new Headers({"Content-Type": "application/json"});

    _urlBase: string;

    constructor(private _http: Http, private resourceUrl: string) {
        this._urlBase = process.env.API_SERVER + resourceUrl;
    }

    /**
     * Creates a new resource record.
     *
     * @param resource Resource to be created
     * @returns Resolves with the created resource, Rejects with error message
     */
    create(resource: T): Promise<T> {
        return this._http
            .post(this._urlBase, JSON.stringify(resource), {headers: this._headers })
            .toPromise()
            .then(function(response): T {
                return response.json();
            }).catch(function(response): any {
                return Promise.reject(response.json());
            });
    }

    /**
     * Fetches the specified resource.
     *
     * @param id Tracking id of the resource
     * @returns Resolves with the created resource, Rejects with error message
     */
    read(id: string): Promise<T> {
        const url = `${this._urlBase}${id}/`;

        return this._http
            .get(url)
            .toPromise()
            .then(response => response.json())
            .catch(error => Promise.reject(error.json()));
    }

    /**
     * Updates the specified resource.
     *
     * @param id Tracking id of the resource
     * @param resource Updated value of the resource
     * @returns Resovles with the updated resource, Rjects with error message
     */
    update(id: string, resource: T): Promise<T> {
        const url = `${this._urlBase}${id}/`;

        return this._http
            .put(url, JSON.stringify(resource), {headers: this._headers })
            .toPromise()
            .then(response => response.json())
            .catch(error => Promise.reject(error.json()));
    }
}
