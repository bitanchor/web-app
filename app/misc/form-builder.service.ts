/**
 * @author R. Matt McCann
 * @brief Service that assists in building standard forms
 * @copyright &copy; 2017 R. Matt McCann
 */

import { Injectable } from "@angular/core";
import { FormGroup } from "@angular/forms";


/** The state of an individual field within the form. */
export interface FieldState {
    error: Array<string>;
    accepted: boolean;
};

/** Providers functionality for building the standard form objects used throughout the app. */
@Injectable()
export class FormBuilderService {

    public moneyRegex = "[\$][0-9]+(,[0-9]{3})*";

    public naturalEnglishRegex = "[-.\" a-zA-Z]+";

    public numericRegex = "[0-9]+";

    public emailRegex = "[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*";

    /**
     * Builds the form state object used to hold error messages and acceptance status.
     *
     * @param fields A list of field names whose state is to be tracked
     * @returns The populated form state object
     */
    buildFormState(fields: Array<string>): Map<string, FieldState> {
        let formState = new Map<string, FieldState>();

        for (let iter = 0; iter < fields.length; iter++) {
            let fieldState = {
                error: new Array<string>(),
                accepted: false
            };

            formState[fields[iter]] = fieldState;
        }

        return formState;
    }

    buildValidatorMessages(field: string, validators: Array<any>): Map<string, string> {
        let validatorMessages = new Map<string, string>();

        for (let iter = 0; iter < validators.length; iter++) {
            let validatorKey = validators[iter];
            let validatorOption = undefined;

            if (validatorKey instanceof Array) {
                validatorOption = validatorKey[1];
                validatorKey = validatorKey[0];
            }

            if (validatorKey === "required") {
                validatorMessages[validatorKey] = this._humanizeFieldName(field) + " is required";
            } else if (validatorKey === "minlength") {
                validatorMessages[validatorKey] = this._humanizeFieldName(field) +
                    " must be at least " + validatorOption + " characters long";
            } else if (validatorKey === "maxlength") {
                validatorMessages[validatorKey] = this._humanizeFieldName(field) +
                    " cannot be more than " + validatorOption + " chacters long";
            } else if (validatorKey === "pattern") {
                validatorMessages[validatorKey] = this._humanizeFieldName(field) +
                    " does not appear to be valid";
            } else {
                throw RangeError("Unknown validator key type '" + validatorKey + "'");
            }
        }

        return validatorMessages;
    }

    groupClass(field: FieldState): any {
        return {
            "has-error": field.error.length,
            "has-success": field.accepted,
            "has-feedback": (field.error.length || field.accepted)
        };
    }

    _humanizeFieldName(field: string): string {
        if (field.length === 0) {
            throw RangeError("field must not be empty string!");
        }

        let woUnderscore = field.replace(/_/g, " ");

        return woUnderscore.charAt(0).toUpperCase() + woUnderscore.slice(1);
    }

    /**
     * Provides a standard method for processing form changes.
     *
     * @param form The form that changed
     * @param formState The ouput container that holds form state information
     * @param validationMessages Messages to be displayed when a validator is violated
     */
    onFormChanged(
            form: FormGroup, formState: Map<string, FieldState>,
            validationMessages: Map<string, Map<string, string>>) {
        for (const field in formState) {
            if (formState.hasOwnProperty(field)) {
                // Clean previous error message (if any)
                formState[field].error = Array<string>();
                formState[field].accepted = false;
                const control = form.get(field);

                // The field has changed and is invalid
                if (control && control.dirty && !control.valid) {
                    // Set the error message
                    const messages = validationMessages[field];
                    for (const key in control.errors) {
                        if (control.errors.hasOwnProperty(key)) {
                            formState[field].error.push(messages[key]);
                        }
                    }
                } else if (control && control.dirty && control.valid) {
                    formState[field].accepted = true;
                }
            }
        }
    }
}
