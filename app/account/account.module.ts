/***********************************************************************************************************************
 * @blame R. Matt McCann
 * @brief Module for the account services & ui
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule} from "@angular/forms";

import { SharedModule } from "../misc/shared.module";
import { AuthService } from "./auth.service";
import { LoginComponent } from "./login.component";

export let MODULE_DEF = {
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule
    ],
    declarations: [
        LoginComponent
    ],
    exports: [
        LoginComponent
    ],
    providers: [
        AuthService
    ]
};

@NgModule(MODULE_DEF)
export class AccountModule { }
