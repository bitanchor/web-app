/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief Basic vendor portal login page
 * @copyright &copy; 2017 Evil Corp.
 **********************************************************************************************************************/

import "rxjs/add/operator/debounceTime";

import { Component, Input } from "@angular/core";
import { FormBuilder, FormControl, Validator, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";

import { UserService } from "../admin/user.service";
import { FormBuilderService } from "../misc/form-builder.service";
import { ModalFormComponent } from "../misc/modal-form.component";
import { AuthService } from "./auth.service";


@Component({
    selector: "login",
    templateUrl: "login.component.html",
    styleUrls: ["login.component.scss"]
})
export class LoginComponent extends ModalFormComponent {
    _isFirstInit = true;

    /** Error message revealed indicating the user"s login crednetials were incorrect. */
    _loginFailed: string;

    loginCompleted = false;

    /** TODO: Hard-coding for the demo. */
    userId = "0";

    /** Url to redirect to after a successful login. */
    @Input() public redirectUrl: Array<string>;

    constructor(
        private _activatedRoute: ActivatedRoute,
        private _authService: AuthService,
        _formBuilder: FormBuilderService,
        private _ngFormBuilder: FormBuilder,
        private _router: Router,
        private userService: UserService
    ) {
        super(_formBuilder);
    }

    /** Initialize the component. */
    _init(): void {
        if (this._isFirstInit) {
            this._isFirstInit = false;

            let me = this;
            this._activatedRoute.queryParams.subscribe(params => {
                if (params["context_logout"]) {
                    me._loginFailed = "You were logged out because you are no longer at \"Drive Capital - Partner's Conference Room\"!";
                }
            });
        }

        // Build the form state tracker
        this._formState = this._formBuilder.buildFormState(["email", "password", "remember_me"]);

        // Configure the form
        this._validationMessages["email"] =
            this._formBuilder.buildValidatorMessages("email", ["required"]);
        this._validationMessages["password"] =
            this._formBuilder.buildValidatorMessages("password", ["required"]);

        // Define the validation rules for the form
        let validationRules = new Map<string, Array<Validator>>();
        validationRules["email"] = [Validators.required];
        validationRules["password"] = [Validators.required];

        // Build the form
        let formGroup = {
            "email": new FormControl("", validationRules["email"]),
            "password": new FormControl("", validationRules["password"]),
            "remember_me": new FormControl(false, [])
        };
        this._form = this._ngFormBuilder.group(formGroup);

        // Subscribe to form value changes
        this._subscribeToFormChanges();
    }

    /** Process changes in the input form. */
    _onFormChanged(): void {
        // Process the form changes
        this._formBuilder.onFormChanged(this._form, this._formState, this._validationMessages);
    }

    /** Process the form submission. */
    _onSubmit(): void {
        // Clear the error messages
        this._loginFailed = undefined;
        this._unexpectedError = false;

        // Try to log the user in
        this._authService.login(this._form.value.email, this._form.value.password, this._form.value.remember_me)
            .then(() => {
                // Reset the form contents
                this._form.reset();

                // Clear the login error
                this._loginFailed = undefined;

                // Redirect to the internal portal page
                // this._router.navigate(["portal"]);

                // Update the user record
                this.userService.read(this.userId)
                    .then((user) => {
                        user.is_authenticated = true;

                        this.userService.update(this.userId, user)
                            .then((user) => {
                                // Reveal the login complete message
                                this.loginCompleted = true;
                            });
                    });
            }, (msg) => {
                this._loginFailed = msg;
            });
    }
}
