/***********************************************************************************************************************
 * @blame R. Matt McCann <mccann.matt@gmail.com>
 * @brief CRUD service for the account model
 * @copyright &copy; 2017 R. Matt McCann
 **********************************************************************************************************************/

import { Injectable } from "@angular/core";


@Injectable()
export class AuthService {

    // is_in_context = false;
    is_in_context = true;

    login(email: string, password: string, remember_me: boolean): Promise<string> {
        if ((email === "matt@nationwide.com") && (password === "Password123")) {
            if (this.is_in_context) {
                localStorage.setItem("is_logged_in", "true");

                return Promise.resolve("Success");
            } else {
                return Promise.reject("Cannot log in - You must be at \"Drive Capital - Partner's Conference Room\"!");
            }
        } else {
            return Promise.reject("Unrecognized email/password combo!");
        }
    }

    logout(): void {
        delete localStorage["is_logged_in"];
    }
}
